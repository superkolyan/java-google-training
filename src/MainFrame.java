import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
/*
 * Created by JFormDesigner on Sun May 04 11:13:02 NOVT 2014
 */



/**
 * @author asd
 */
public class MainFrame extends JFrame {
    enum Languages{
        ARABIC_JORDAN("ar-JO"),
        ARABIC_LEBANON("ar-LB"),
        ARABIC_QATAR("ar-QA"),
        ARABIC_UAE("ar-AE"),
        ARABIC_MOROCCO("ar-MA"),
        ARABIC_IRAQ("ar-IQ"),
        ARABIC_ALGERIA("ar-DZ"),
        ARABIC_BAHRAIN("ar-BH"),
        ARABIC_LYBIA("ar-LY"),
        ARABIC_OMAN("ar-OM"),
        ARABIC_SAUDI_ARABIA("ar-SA"),
        ARABIC_TUNISIA("ar-TN"),
        ARABIC_YEMEN("ar-YE"),
        BASQUE("eu"),
        CATALAN("ca"),
        CZECH("cs"),
        DUTCH("nl-NL"),
        ENGLISH_AUSTRALIA("en-AU"),
        ENGLISH_CANADA("en-CA"),
        ENGLISH_INDIA("en-IN"),
        ENGLISH_NEW_ZEALAND("en-NZ"),
        ENGLISH_SOUTH_AFRICA("en-ZA"),
        ENGLISH_UK("en-GB"),
        ENGLISH_US("en-US"),
        FINNISH("fi"),
        FRENCH("fr-FR"),
        GALICIAN("gl"),
        GERMAN("de-DE"),
        HEBREW("he"),
        HUNGARIAN("hu"),
        ICELANDIC("is"),
        ITALIAN("it-IT"),
        INDONESIAN("id"),
        JAPANESE("ja"),
        KOREAN("ko"),
        LATIN("la"),
        CHINESE_SIMPLIFIED("zh-CN"),
        CHINESE_TRANDITIONAL("zh-TW"),
        CHINESE_HONGKONG("zh-HK"),
        CHINESE_CANTONESE("zh-yue"),
        MALAYSIAN("ms-MY"),
        NORWEGIAN("no-NO"),
        POLISH("pl"),
        PIG_LATIN("xx-piglatin"),
        PORTUGUESE("pt-PT"),
        PORTUGUESE_BRASIL("pt-BR"),
        ROMANIAN("ro-RO"),
        RUSSIAN("ru"),
        SERBIAN("sr-SP"),
        SLOVAK("sk"),
        SPANISH_ARGENTINA("es-AR"),
        SPANISH_BOLIVIA("es-BO"),
        SPANISH_CHILE("es-CL"),
        SPANISH_COLOMBIA("es-CO"),
        SPANISH_COSTA_RICA("es-CR"),
        SPANISH_DOMINICAN_REPUBLIC("es-DO"),
        SPANISH_ECUADOR("es-EC"),
        SPANISH_EL_SALVADOR("es-SV"),
        SPANISH_GUATEMALA("es-GT"),
        SPANISH_HONDURAS("es-HN"),
        SPANISH_MEXICO("es-MX"),
        SPANISH_NICARAGUA("es-NI"),
        SPANISH_PANAMA("es-PA"),
        SPANISH_PARAGUAY("es-PY"),
        SPANISH_PERU("es-PE"),
        SPANISH_PUERTO_RICO("es-PR"),
        SPANISH_SPAIN("es-ES"),
        SPANISH_US("es-US"),
        SPANISH_URUGUAY("es-UY"),
        SPANISH_VENEZUELA("es-VE"),
        SWEDISH("sv-SE"),
        TURKISH("tr"),
        ZULU("zu");
        private final String languageCode;

        Languages(String languageCode) {
          this.languageCode=languageCode;

        }
        public String toString() {
            return languageCode;
        }
    }
    VoiceRecorder VR=new VoiceRecorder();
    SpeachRecognizerGoogle speachRecognizerGoogle =new SpeachRecognizerGoogle();
    StringParcer stringParcer=new StringParcer();
    TTSGoogle ttsGoogle=new TTSGoogle();
    TranslatorGoogle translatorGoogle=new TranslatorGoogle();
       MainFrame() {
        setVisible(true);
        initComponents();
        comboBox1.setModel(new DefaultComboBoxModel(Languages.values()));
           System.out.print("Hi");
    }


    private void toggleButton1MouseClicked(MouseEvent e) {
        if(toggleButton1.isSelected()){
            VR.RecordThat();
        }else {
            VR.stopped=true;
            VR.finish();
            String[] tempSTr=stringParcer.parceEverything(speachRecognizerGoogle.sendRequest(VR.getOutputStream()));
            if(tempSTr.equals(null)){
                textField1.setText("ERROR");
            }else {
                textField1.setText(tempSTr[0]);
                label1.setText("<html>Вероятность<br> распознования:<br>"+tempSTr[1]+ "</html>");
            }
            }

        }

        private void button2MousePressed(MouseEvent e) {
            if(textField2.getText().length()>0) {
                ttsGoogle.TextSender(textField2.getText(),String.valueOf(comboBox1.getSelectedItem()));
            }
        }

        private void button1MousePressed(MouseEvent e) {
            if(textField1.getText()!=null){
                textField2.setText(translatorGoogle.GoogleTranslation(textField1.getText(),String.valueOf(comboBox1.getSelectedItem())));
            }else System.out.println("ERROR TEXTFIELD IS EMPTY");
        }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        textField1 = new JTextField();
        textField2 = new JTextField();
        button1 = new JButton();
        toggleButton1 = new JToggleButton();
        comboBox1 = new JComboBox();
        button2 = new JButton();
        label1 = new JLabel();

        //======== this ========
        setTitle("Google api examples");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/FrameIcon.png")).getImage());
        Container contentPane = getContentPane();

        //---- textField1 ----
        textField1.setHorizontalAlignment(SwingConstants.LEFT);

        //---- textField2 ----
        textField2.setHorizontalAlignment(SwingConstants.LEFT);

        //---- button1 ----
        button1.setText("Translate");
        button1.setBackground(Color.lightGray);
        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                button1MousePressed(e);
            }
        });

        //---- toggleButton1 ----
        toggleButton1.setSelectedIcon(new ImageIcon(getClass().getResource("/VoicRecSelected.png")));
        toggleButton1.setIcon(new ImageIcon(getClass().getResource("/VoiceRec.png")));
        toggleButton1.setBackground(Color.lightGray);
        toggleButton1.setForeground(Color.lightGray);
        toggleButton1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                toggleButton1MouseClicked(e);
            }
        });

        //---- comboBox1 ----
        comboBox1.setBackground(Color.lightGray);

        //---- button2 ----
        button2.setIcon(new ImageIcon(getClass().getResource("/speakericon.png")));
        button2.setBackground(Color.lightGray);
        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                button2MousePressed(e);
            }
        });

        //---- label1 ----
        label1.setText("<html>\u0412\u0435\u0440\u043e\u044f\u0442\u043d\u043e\u0441\u0442\u044c<br> \u0440\u0430\u0441\u043f\u043e\u0437\u043d\u043e\u0432\u0430\u043d\u0438\u044f:</html>");
        label1.setVerticalAlignment(SwingConstants.TOP);
        label1.setFont(new Font("Dialog", Font.BOLD, 10));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(textField1, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                            .addGap(19, 19, 19)
                            .addComponent(textField2, GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(toggleButton1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(button1, GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                            .addGap(18, 18, 18)
                            .addComponent(comboBox1, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(button2, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                            .addGap(33, 33, 33))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(textField1, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                        .addComponent(textField2, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(toggleButton1)
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                        .addComponent(button1, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(button2, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(14, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JTextField textField1;
    private JTextField textField2;
    private JButton button1;
    private JToggleButton toggleButton1;
    private JComboBox comboBox1;
    private JButton button2;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

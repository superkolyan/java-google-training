import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by super_000 on 02.05.2014.
 */
public class SpeachRecognizerGoogle {
    public String sendRequest(ByteArrayOutputStream ByteOutStream) {

        //System.setProperty("https.proxyHost", "127.0.0.1");
        //System.setProperty("https.proxyPort", "8888");
        try {
            byte[] data = ByteOutStream.toByteArray();
            URL googleSpeech = new URL("https://www.google.com/speech-api/v2/recognize?lang=ru-RU&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw&results=6&pfilter=2");
            HttpsURLConnection googleConnect = (HttpsURLConnection)googleSpeech.openConnection();
            System.out.println("Connection opened...");
            googleConnect.setRequestMethod("POST");
            googleConnect.setDoOutput(true);
            googleConnect.setDoInput(true);
            googleConnect.setUseCaches(false);
            googleConnect.setRequestProperty("Content-Type", "audio/x-flac; rate=16000");
            googleConnect.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7");
            DataOutputStream wr = new DataOutputStream(googleConnect.getOutputStream());
            wr.write(data);
            wr.flush();
            wr.close();
            BufferedReader rd = new BufferedReader(new InputStreamReader(googleConnect.getInputStream()));
            String line = rd.readLine();
            rd.close();
            googleConnect.disconnect();
            System.out.println("Connection closed...");
            return line;
        } catch (IOException e) {
            e.printStackTrace();
        }return "ERROR";
    }
}

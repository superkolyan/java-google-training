import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by super_000 on 04.05.2014.
 */
public class TranslatorGoogle {

    public String GoogleTranslation(String text,String targetLanguage) {
        try {
            String encodedString= URLEncoder.encode(text,"UTF-8");
            URL googleTranslateURL =new URL("http://translate.google.com/translate_a/t?client=t");
            URL temlUrl=new URL(googleTranslateURL+"&sl=ru&tl=" +targetLanguage+"&text="+encodedString );
            URLConnection googleConnect= temlUrl.openConnection();
            googleConnect.setDoOutput(true);
            googleConnect.setDoInput(true);
            googleConnect.setUseCaches(false);
            googleConnect.setRequestProperty("Content-Length",String.valueOf(text.length()));
            googleConnect.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0");
            BufferedReader rd = new BufferedReader(new InputStreamReader(googleConnect.getInputStream()));
            String line = rd.readLine();
            rd.close();
            String[]out=line.split("\"");
            return out[1];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}


import javaFlacEncoder.FLACFileWriter;

import javax.sound.sampled.*;
import java.io.*;
import java.nio.file.Files;

/**
 * Created by super_000 on 02.05.2014.
 */
public class VoiceRecorder  {
    boolean stopped;
    TargetDataLine tempLine;
    private ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
    File flacFile;
    AudioInputStream ais;

    AudioFormat getAudioFormat() {
        float sampleRate = 16000.f;
        int sampleSizeInBits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                channels, signed, bigEndian);
        return format;
    }

    public void RecordThat(){
        AudioFormat format = getAudioFormat();
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        new File("C:/Users/"+System.getProperty("user.name")+"/JavaGoogleAPI/").mkdirs();
        flacFile=new File("C:/Users/"+System.getProperty("user.name")+"/JavaGoogleAPI/temp.flac");
        outputStream.reset();

        if (!AudioSystem.isLineSupported(info)) {
            System.out.println("Line not supported");
            System.exit(0);
        }
        System.out.println("Start recording...");
        try {
            tempLine = (TargetDataLine) AudioSystem.getLine(info);
            tempLine.open(format);
            tempLine.start();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
             ais=new AudioInputStream(tempLine);
        Thread writingThread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                     AudioSystem.write(ais, FLACFileWriter.FLAC, outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        writingThread.start();
    }

    void finish() {
        tempLine.stop();
        tempLine.close();
        try {
            ais.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished...");
    }

    public void PlayThat() throws LineUnavailableException, IOException {
        AudioFormat format = getAudioFormat();
        byte audio[] = outputStream.toByteArray();
        InputStream input = new ByteArrayInputStream(audio);
        AudioInputStream ais = new AudioInputStream(input,
                format, audio.length / format.getFrameSize());
        DataLine.Info info = new DataLine.Info(
                SourceDataLine.class, format);
        SourceDataLine line =
                (SourceDataLine)AudioSystem.getLine(info);
        line.open(format);
        line.start();
        int bufferSize = (int) format.getSampleRate()
                * format.getFrameSize();
        byte buffer[] = new byte[bufferSize];

        int count;
        while ((count =
                ais.read(buffer, 0, buffer.length)) != -1) {
            if (count > 0) {
                line.write(buffer, 0, count);
            }
        }
        line.drain();
        line.close();
    }

    public ByteArrayOutputStream getOutputStream(){
        return outputStream;
    }


}

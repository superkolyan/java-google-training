import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by super_000 on 03.05.2014.
 */
public class TTSGoogle {
    public void TextSender(String string,String targetLanguage){
        new File("C:/Users/"+System.getProperty("user.name")+"/JavaGoogleAPI/").mkdirs();
        File mp3=new File("C:/Users/"+System.getProperty("user.name")+"/JavaGoogleAPI/temp.mp3");
        try {
            String encode= URLEncoder.encode(string, "UTF-8");
            URL TTSURL=new URL("http://translate.google.com/translate_tts?tl="+targetLanguage+"&ie=UTF8&q="+encode);
            HttpURLConnection TTSConnection = (HttpURLConnection) TTSURL.openConnection();
            TTSConnection.setRequestMethod("GET");
            TTSConnection.setDoOutput(true);
            TTSConnection.setDoInput(true);
            TTSConnection.setRequestProperty("User-Agent","Fiddler");
            FileOutputStream fileOutputStream=new FileOutputStream(mp3);
            InputStream inputStream=TTSConnection.getInputStream();
            byte[]buffer =new byte[8196];
            int length;
            while((length=inputStream.read(buffer))>0){
                fileOutputStream.write(buffer,0,length);
            }
            TTSConnection.disconnect();
            fileOutputStream.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXPanel fxPanel = new JFXPanel();
        Media media=new Media(mp3.toURI().toString());
        MediaPlayer mediaPlayer=new MediaPlayer(media);
        mediaPlayer.play();
        if(mp3.exists()){
            mp3.setWritable(true);
            System.out.println(mp3.delete());
            mp3.deleteOnExit();
        }

    }

}
